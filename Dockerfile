# syntax=docker/dockerfile:1
FROM alpine:latest AS build
# get arm toolchain
RUN wget -qO- https://developer.arm.com/-/media/Files/downloads/gnu-a/10.3-2021.07/binrel/gcc-arm-10.3-2021.07-x86_64-aarch64-none-linux-gnu.tar.xz | tar xJ

FROM ubuntu:20.04

ARG USERNAME=dockeruser
ARG UID=1001
ARG GID=1001

# install yocto packages and additional packages for quality of life
RUN apt-get -y update && \
    DEBIAN_FRONTEND=noninteractive apt-get -y install \
    gawk \
    wget \
    git \
    diffstat \
    unzip \
    texinfo \
    gcc \
    build-essential \
    chrpath \
    socat \
    cpio \
    python3 \
    python3-pip \
    python3-pexpect \
    xz-utils \
    debianutils \
    iputils-ping \
    python3-git \
    python3-jinja2 \
    libegl1-mesa \
    libsdl1.2-dev \
    pylint3 \
    xterm \
    python3-subunit \
    mesa-common-dev \
    zstd \
    liblz4-tool \
    locales \
    sudo \
    vim \
    bash-completion \
    linux-headers-5.4.0-99-generic

COPY requirements.txt /tmp/
# get kas tool for automating bitbake
RUN pip3 install -r /tmp/requirements.txt

RUN /usr/sbin/locale-gen en_US.UTF-8 && \
    echo "source /etc/profile.d/bash_completion.sh" >> ~/.bashrc

ENV LANG en_US.UTF-8
# make a none root user
RUN mkdir /home/${USERNAME} \
    && chown -R ${UID}:${GID} /home/${USERNAME} \
    && groupadd -g ${GID} ${USERNAME} \
    && useradd -N -u ${UID} -g ${GID} ${USERNAME} \
    && echo ${USERNAME} ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/${USERNAME} \
    && chmod 0440  /etc/sudoers.d/${USERNAME}

USER ${USERNAME}

# make github a known host 
RUN mkdir /home/${USERNAME}/.ssh \
    && touch /home/${USERNAME}/.ssh/known_hosts \
    && chmod 0600 /home/${USERNAME}/.ssh/known_hosts \
    && ssh-keyscan -t rsa github.com >> /home/${USERNAME}/.ssh/known_hosts

# copy over arm toolchain
COPY --from=build ./gcc-arm-10.3-2021.07-x86_64-aarch64-none-linux-gnu /home/${USERNAME}/gcc-arm-10.3-2021.07-x86_64-aarch64-none-linux-gnu
ENV PATH="/home/${USERNAME}/gcc-arm-10.3-2021.07-x86_64-aarch64-none-linux-gnu/bin:$PATH"

WORKDIR /home/${USERNAME}

ENTRYPOINT ["/bin/bash", "-c"]
