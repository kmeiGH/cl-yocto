#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>

MODULE_LICENSE("Dual BSD/GPL");
MODULE_VERSION("0.0.0");
MODULE_DESCRIPTION("Hello world driver");

/*
** Init function
*/
static int hello_init(void)
{
    printk("Hello world, this is a test for module\n");
    return 0;
}

/*
** Exit function
*/
static void hello_exit(void)
{
    printk("Goodbye world, our test succeeded!\n");
}

module_init(hello_init);
module_exit(hello_exit);
        