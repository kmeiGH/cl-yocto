
# kas

## Official documentation - [link](https://kas.readthedocs.io/en/latest/intro.html)

## Introduction
Read the official documentation before you begin.

kas needs to be installed via pip: `sudo pip3 install .`. kas supports two modes:

`kas build /path/to/kas-project.yml` which will initiate a bitbake command in the current directory, OR

`kas shell /path/to-kas-project.yml` which will initiate a new shell environment with a `build/` directory and all of the `conf/` files needed for you to run bitbake commands.

An example kas configuration is highlighted below. Some key takeaways:
* the machine and distro relate to parameters in `local.conf`
* values under `bblayers_conf_header:` and `local_conf_header:` are appended to `bblayers.conf` and `local.conf`, respectively. This is how kas can help you automate setting up these parameters so you don't have to manually add them in yourself.
* under `repos:`, you specify the layers that you want to run. It is importatnt to note here that you can either select absolute directories in your machine to locate the layers OR you can choose git repositories (git urls). `refspec` lets you pin the commit hash so that you always checkout into that specific commit for that layer. *Currently, refspec seems to only work for git urls and not for local repos identified in path*. This is a powerful feature as when you have a lot of layers, this can become error prone.
* by default when you run `kas build /path/to-kas-project.yml` it selects `core-image-minmal`. However, you can change this behavior by adding the name of your own image behind the `target` key.
```yaml
header:
  version: 11
machine: qemux86-64
distro: poky
# target: # this defaults to core-image-minimal. change this to update to your image
repos:
  meta-mylayer:
    path: /workspaces/cl-yocto/meta-mylayer
  poky:
    url: "https://git.yoctoproject.org/git/poky"
    refspec: 4eea21b7addd091c7e549684699897fe7d60533c
    layers:
      meta:
      meta-poky:
      meta-yocto-bsp:
bblayers_conf_header:
  standard: |
    POKY_BBLAYERS_CONF_VERSION = "2"
    BBPATH = "${TOPDIR}"
    BBFILES ?= ""

local_conf_header:
  standard: |
    CONF_VERSION = "2"

  debug_tweaks: |
    EXTRA_IMAGE_FEATURES = "debug-tweaks"

  diskmon: |
    BB_DISKMON_DIRS ??= "\
      STOPTASKS,${TMPDIR},1G,100K \
      STOPTASKS,${DL_DIR},1G,100K \
      STOPTASKS,${SSTATE_DIR},1G,100K \
      STOPTASKS,/tmp,100M,100K \
      ABORT,${TMPDIR},100M,1K \
      ABORT,${DL_DIR},100M,1K \
      ABORT,${SSTATE_DIR},100M,1K \
      ABORT,/tmp,10M,1K"

  sstate_mirrors: |
    SOURCE_MIRROR_URL = "http://172.19.1.225:8080/downloads"
    INHERIT += "own-mirrors"
    SSTATE_MIRRORS = "file://.* http://172.19.1.225:8080/sstate-cache/PATH"
```
