# Lessons learned

Feel free to add to this document when you find some tips, tricks, or pitfalls.

1. In a docker environment, when you execute runqemu, use the slirp and nographic flags via: `runqemu <machine> slirp nographic`. This prevents some commonly seen errors.
2. Use the bitbake create-layer or devtools command to generate an example layer.
3. When you first have a recipe, it will read `LIC_FILES_CHKSUM = "file://COPYING;md5=xxx"`. This is a checksum for validating if the license changed or not. Don't worry if you don't know the checksum. Run bitbake and an error will popup with a checksum. Copy and paste it into the file and run bitbake again.
4. You can't run kas in the devcontainer workspaces folder because it doesn't have the correct "case-sensitive" filesystem. This might have something to do with devcontainers mounting the repo as a volume? You SHOULD run kas in the /home/\<your-user\> directory inside the container.
