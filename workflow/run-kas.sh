#!/bin/bash

set -euo pipefail

export KAS_FILE="kas-project.yml"
export HOME="/home/dockeruser/"
# get absolute path to this script
export script_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd -P)"

# init ssh
echo "adding ssh: "
eval "$(ssh-agent -s)"
ssh-add .ssh/secret_key

# run outside volume directory
cd $HOME
echo "script dir: ${script_dir}"

# go up one level and execute kas build
kas build "$script_dir/../${KAS_FILE}"
