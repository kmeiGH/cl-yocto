# Table of Contents
1. [Reference documentation](#reference-documentation)
2. [Getting started](#getting-started)
    1. [Using Docker](#option-a-using-docker-in-devcontainer)
    2. [Using Linux](#option-b-using-a-linux-machine)
3. [Speeding up your builds](#speeding-up-your-builds)
4. [kas - READ ME](#kas)
5. [Lessons learned](./docs/lessons.md)


## Reference documentation
We use the yocto project, an open source software, to build custom linux distributions.

Have a read through the yocto project's <u> [official documentation]((https://docs.yoctoproject.org/brief-yoctoprojectqs/index.html))</u> before you being. Also have a look at the yocto [wiki](https://wiki.yoctoproject.org/wiki/TipsAndTricks).

## Getting started

### Option A Using Docker in devcontainer
#### (Windows/Mac) - [install Docker first](https://www.docker.com/products/docker-desktop).

It is recommended that you build yocto using a docker container. **<u>The devcontainer environment has already been setup for you in this repository!</u>** This isolates the environment to one that has already been configured and verified. The configuration for the devcontainer image is located in the `devcontainer.json` file in this repo.

*Warning: you can try to build your own container to run a devcontainer, and you can find the official docker repository for poky [here](https://github.com/crops/poky-container), but this is not recommended. Follow the steps below to use devcontainer in vscode.*


1. To get set up with devcontainer, look [here](https://code.visualstudio.com/docs/remote/containers-tutorial) for instructions. You need to install the Remote - Containers extension in vscode: 

<img src="./docs/images/containers-extension.png" height="150" width="700">

2. Click on the green button (the two arrows) at the bottom left:

<img src="./docs/images/remote-dev-status-bar.png" height="100" width="200">

3. Finally, click "Remote-Containers: Open Folder in Container" to start editing inside the devcontainer environment:

<img src="./docs/images/remote-containers-commands.png" height="150" width="550">

### Option B Using a Linux Machine
Choose this option if you already have a Linux machine or a VM. The current supported distros for the yocto project are:

* Ubuntu 18.04 (LTS)
* Ubuntu 20.04 (LTS)
* Fedora 33
* Fedora 34
* CentOS 7.x
* CentOS 8.x
* Debian GNU/Linux 8.x (Jessie)
* Debian GNU/Linux 9.x (Stretch)
* Debian GNU/Linux 10.x (Buster)
* openSUSE Leap 15.1
* openSUSE Leap 15.2

Make sure you have available **50GB** of **free disk space**. It's recommended that you allocate at least 100GB.

You need to clone the poky repository (WARNING: you do not need to do this if you are using kas):
`git clone git://git.yoctoproject.org/poky` and you will also need to download this repo.

Then, follow the steps in the yocto [docs](https://docs.yoctoproject.org/brief-yoctoprojectqs/index.html) to initialize your build and to use `bitbake`.

If you run into this error: 
```
ERROR:  OE-core's config sanity checker detected a potential misconfiguration.
    Either fix the cause of this error or at your own risk disable the checker (see sanity.conf).
    Following is the list of potential problems / advisories:

    Your system needs to support the en_US.UTF-8 locale.
```
make sure you enable it by running the command `/usr/sbin/locale-gen en_US.UTF-8`.

## Speeding up your builds
The yocto project takes a long time to build. When you first use bitbake for build an image, even the smallest `core-image-minimal` can take up to 2 hours depending on your machine specifications. This can become a bottleneck for development workflows, so the yocto project gives you some configuratiion options to speed up your build.

This involves setting up `SSTATE` cache and `DL_DIR` directories either locally, or configure your build with a remote cache. 

For more information, see [here](./docs/buildtime.md).

Time it takes to build using downloads + sstate (2min) |  Time it will take to build without (35 min)
:-------------------------:|:-------------------------:
![img](/docs/images/sstate_time.png) | <img src="./docs/images/cim_long_build.png" width="1000">

## kas

kas is an open source tool provided by [siemens](https://github.com/siemens/kas). It is recommended that you get familiar with the workflow of using yocto and bitbake and then come back to kas to understand how it can help you get rid of a lot of the repetitive tasks in a typical workflow.

A kas configuration file is in this repo `kas-project.yml`. See [here](./docs/kas.md) for more information on kas.

*kas will clone the poky repository for you if you specify it in the yaml!*

The tool allows you to automate the process of switching branches between layers, pinning revisions, initialize default `local.conf` and `bblayers.conf` settings for bitbake. It can setup the build environment for you or it can be called to run the bitbake build. 

kas's official documentation reads:
```
This tool provides an easy mechanism to setup bitbake based projects.

...

Key features provided by the build tool:

clone and checkout bitbake layers
create default bitbake settings (machine, arch, …)
launch minimal build environment, reducing risk of host contamination
initiate bitbake build process
```






